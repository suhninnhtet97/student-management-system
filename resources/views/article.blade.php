@extends('layouts.student-layout')
@section('content')
<div class="container mt-4">
        <div class="row">
            <div class="col-lg-8">
                <article>
                    <h1 class="mb-4">Article Title</h1>
                    <p class="text-muted">Published on January 1, 2023 by John Doe</p>
                    <img src="path-to-article-image.jpg" alt="Article Image" class="img-fluid mb-4">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed cursus semper tortor vel consectetur. In hendrerit varius enim ut blandit. Pellentesque at quam justo. Integer tristique euismod risus ut facilisis. Nullam ut rhoncus velit. Sed sit amet justo dapibus, consequat nisl ut, malesuada mauris. Sed sit amet lectus eu nisi suscipit vestibulum. Proin non velit vel libero egestas finibus sed id mauris.</p>
                    <p>Quisque fermentum dolor sit amet nibh egestas, non placerat quam suscipit. Proin et commodo risus. Donec ac turpis sit amet velit convallis dignissim id vitae ipsum. Proin at diam condimentum, laoreet odio non, pulvinar sem. Vivamus laoreet vestibulum eleifend. Cras ac est vel arcu aliquam bibendum.</p>
                </article>
            </div>
        </div>
</div>
@endsection