@extends('errors::minimal')
@section('title', 'Page not found')
@section('code', '404')

@section('code-message','Page not found.')
@section('message','The page you’re looking for doesn’t exist.')