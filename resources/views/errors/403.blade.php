@extends('errors::minimal')

@section('title', __('Forbidden'))
@section('code', '403')
@section('message', __($exception->getMessage() ?: 'Forbidden'))

@extends('errors::minimal')
@section('title', 'Forbidden')
@section('code', '403')
@section('code-message','Forbidden!')
@section('message',$exception->getMessage() ?: 'Forbidden')
