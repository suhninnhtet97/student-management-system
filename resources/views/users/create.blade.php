@extends('layouts.admin-layout')
@section('content')
<!-- resources/views/users/create.blade.php -->
<h1>Create User</h1>
<form action="{{ route('users.store') }}" method="POST">
    @csrf
    <div class="mb-3">
    <label for="name" class="form-label">Name:</label>
    <input type="text" class="form-control" name="name" id="name" placeholder="Enter user name" required>
    <div>

    <div class="mb-3"> 
    <label for="email" class="form-label">Email:</label>
    <input type="email" class="form-control" name="email" id="email" required>
    </div>

    <div class="mb-3"> 
    <label for="role" class="form-label">Role:</label>
    <select name="role" id="role" class="form-control">
        <option value="admin" disabled>Admin</option>
        <option value="student" default>Student</option>
    </select>
    </div>

    <div class="mb-3"> 
    <label for="password" class="form-label">Password:</label>
    <input type="password" class="form-control" name="password" id="password" required>
    </div>

    <button type="submit" class="btn btn-primary">Create</button>
</form>
@endsection

