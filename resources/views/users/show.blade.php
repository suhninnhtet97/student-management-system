
@extends('layouts.admin-layout')
@section('content')
<!-- resources/views/users/show.blade.php -->
<div class="container-fluid">
    <div class="row">
        
            <h2>User Details</h2>
        
    </div>
    <div class="row">
        <p><strong>Name:</strong> {{ $user->name }}</p>
    </div>
    <div class="row">
        <p><strong>Email:</strong> {{ $user->email }}</p>
    </div>
    <div class="row">
        <p><strong>Role:</strong> {{ $user->role }}</p>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <a href="{{ route('users.edit', $user->id) }}" class="btn btn-primary">Edit</a>
        </div>
        <div class="col-sm-4">
            <form action="{{ route('users.destroy', $user->id) }}" method="POST">
                @csrf
                @method('DELETE')
                <button type="submit"class="btn btn-danger">Delete</button>
            </form>
        </div>
    </div>
</div>
@endsection