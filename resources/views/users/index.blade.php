
@extends('layouts.admin-layout')
@section('content')
<!-- resources/views/users/index.blade.php -->
<div class="container">
    <div class="row">
        <div class="col-sm-6">
            <h1>User List</h1>
        </div>
       
    </div>
    <div class="row">
    <table class ="table table-striped table-bordered">
    <thead>
        <tr>
            <th class="text-center" scope = "col" >ID</th>
            <th class="text-center" scope = "col">Name</th>
            <th class="text-center" scope = "col">Email</th>
            <th class="text-center" scope = "col">Role</th>
            <th  class="text-center" scope = "col" colspan="3">Actions   <a href="{{ route('users.create') }}" class="btn btn-link">Create User</a></th>
        </tr>
    </thead>
    <tbody>
        @foreach($users as $user)
        <tr>
        <th scope="row">{{ $user->id }}</th>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->role }}</td>
            <td>
                <a href="{{ route('users.show', $user->id) }}" class="btn btn-success">View</a>
            </td>
            <td>    
                <a href="{{ route('users.edit', $user->id) }}" class="btn btn-primary">Edit</a>
            </td>
            <td>
                <form action="{{ route('users.destroy', $user->id) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
    </div>
</div>

@endsection


