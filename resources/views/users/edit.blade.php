@extends('layouts.admin-layout')
@section('content')
<!-- resources/views/users/edit.blade.php -->
<h1>Edit User</h1>
<form action="{{ route('users.update', $user->id) }}" method="POST">
    @csrf
    @method('PUT')
    <div class="mb-3">
    <label for="name" class="form-label">Name:</label>
    <input type="text" class="form-control" name="name" id="name" value="{{ $user->name }}" required>
    </div>
    <div class="mb-3">
    <label for="email" class="form-label">Email:</label>
    <input type="email" class="form-control" name="email" id="email" value="{{ $user->email }}" required>
    </div>

    <div class="mb-3">
    <label for="role"class="form-label">Role:</label>
    <select name="role" class="form-control" id="role">
        <option value="admin" {{ $user->role === 'admin' ? 'selected' : '' }}>Admin</option>
        <option value="student" {{ $user->role === 'student' ? 'selected' : '' }}>Student</option>
    </select>
    </div>
    <button type="submit"class="btn btn-primary">Update</button>
</form>
@endsection