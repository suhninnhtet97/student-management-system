@extends('layouts.student-layout')

@section('content')
<div id="app">
    <div class="container" >
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Students') }}</div>

                    <div class="card-body">
                        {{ __('This is student page....') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
