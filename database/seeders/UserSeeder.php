<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'Admin',
                'email' => 'admin@gmail.com',
                'password' => bcrypt('11111111'),
                'role' => 'admin'
            ],
            [
                'name' => 'Student',
                'email' => 'student@gmail.com',
                'password' => bcrypt('11111111'),
                'role' => 'student'
            ]

        ];

        foreach($users as $user) {
            User::create($user);
        }
    }
}
