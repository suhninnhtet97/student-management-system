<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\UserController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// for admin
Route::group(['middleware' => ['auth:web','is_admin']], function () {

    
    // Route::get('/users', function () {
    //     // Perform some logic or processing here if needed
        
    //     return redirect()->route('redirected.route');
    // });

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('admin');
    //Route::get('/users', [App\Http\Controllers\UserController::class, 'index'])->name('users');

        // Route for displaying all users
    Route::get('/users', [UserController::class, 'index'])->name('users.index');

    // Route for displaying the form to create a user
    Route::get('/users/create', [UserController::class, 'create'])->name('users.create');

    // Route for storing a new user
    Route::post('/users', [UserController::class, 'store'])->name('users.store');

    // Route for displaying a specific user
    Route::get('/users/{user}', [UserController::class, 'show'])->name('users.show');

    // Route for displaying the form to edit a user
    Route::get('/users/{user}/edit', [UserController::class, 'edit'])->name('users.edit');

    // Route for updating a specific user
    Route::put('/users/{user}', [UserController::class, 'update'])->name('users.update');

    // Route for deleting a specific user
    Route::delete('/users/{user}', [UserController::class, 'destroy'])->name('users.destroy');




});

// for student
Route::group(['middleware' => ['auth:web','is_student']], function () {

    Route::get('/student-home', function () {
        return view('student-home');
    })->name('student');
    


});

Route::get('/', function () {
    return view('welcome');
});

//Article route
Route::get('/article', function () {
    return view('article');
});

Auth::routes();




